#include "config_settings.h"

#include "dconfig.h"

Configure(config_libsettings);
NotifyCategoryDef(libsettings, "");

ConfigureFn(config_libsettings) {
  init_libsettings();
}

void init_libsettings() {
  static bool initialized = false;
  if (initialized) {
    return;
  }

  initialized = true;
}