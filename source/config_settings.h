#pragma once

#include "pandabase.h"

#include "notifyCategoryProxy.h"

#include "settingsFile.h"

NotifyCategoryDecl(libsettings, EXPORT_CLASS, EXPORT_TEMPL);

extern void init_libsettings();